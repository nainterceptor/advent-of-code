import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

const [part] = Deno.args;

const data: number[][] = stdin
  .split("\n")
  .map((l) => l.split("").map((n) => parseInt(n, 10)));

const height = data.length;
const width = data.at(0)!.length;

const CLOSEST = [
  [+1, 0],
  [-1, 0],
  [0, +1],
  [0, -1],
];

function isLowest(
  grid: number[][],
  x: number,
  y: number,
): boolean {
  return CLOSEST
    .map(([mx, my]) => (grid[x + mx] ?? [])[y + my] ?? Infinity)
    .every((other) => grid.at(x)!.at(y)! < other);
}

function getLowest(grid: number[][]) {
  return grid.flatMap((r, rx) =>
    r.map((_, ry) => [rx, ry])
      .filter(([x, y]) => isLowest(grid, x, y))
  );
}

switch (part) {
  case "part1": {
    const score = getLowest(data).map(([x, y]) => data[x][y] + 1)
      .reduce((a, b) => a + b, 0);
    // expected output from ../input.txt : 554
    await Deno.stdout.write(
      new TextEncoder().encode(
        score.toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    const score = getLowest(data).map(([x, y]) => {
      const open: number[][] = [[x, y]];
      const close: number[][] = [];
      while (open.length > 0) {
        const [row, col] = open.shift()!;
        open.push(
          ...CLOSEST.map(([y, x]) => [row + y, col + x]).filter(([y, x]) =>
            0 <= y && y < height && 0 <= x && x < width &&
            !close.some(([ry, rx]) => ry === y && rx === x) &&
            !open.some(([ry, rx]) => ry === y && rx === x) &&
            data[y][x] < 9
          ),
        );
        close.push([row, col]);
      }
      return close.length;
    })
      .sort((a, b) => b - a)
      .slice(0, 3)
      .reduce((a, b) => a * b, 1);
    // expected output from ../input.txt : 1017792
    await Deno.stdout.write(
      new TextEncoder().encode(
        score.toFixed(),
      ),
    );
    break;
  }
  default:
}
