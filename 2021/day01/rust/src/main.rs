use std::env;
use std::io::{self, BufRead};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let input: Vec<u16> = io::stdin()
        .lock()
        .lines()
        .map(|l| l.unwrap())
        .map(|l| l.parse::<u16>().unwrap())
        .collect();

    match part.as_ref() {
        "part1" => {
            let mut acc: u16 = 0;
            for (index, val) in input.iter().enumerate() {
                if index == 0 {
                    continue;
                }
                if val > &input[index - 1] {
                    acc += 1
                }
            }
            // expected output from ../input.txt : 1288
            print!("{}", acc);
        }
        "part2" => {
            let mut acc: u16 = 0;
            for (index, val) in input.iter().enumerate() {
                if [0, 1, input.len() - 1].contains(&index) {
                    continue;
                }
                let previous_sum = input[index - 2] + input[index - 1] + val;
                let current_sum = input[index - 1] + val + input[index + 1];
                if current_sum > previous_sum {
                    acc += 1;
                }
            }
            // expected output from ../input.txt : 1311
            print!("{}", acc);
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
