import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

const data: number[] = stdin.split("\n").filter((r) => r.length > 0).map((r) =>
  parseInt(r.trim(), 10)
);
const [part] = Deno.args;

const total = data.reduce((acc, val, index, array) => {
  switch (part) {
    case "part1": {
      if (index === 0) {
        return acc;
      }
      // expected output from ../input.txt : 1288
      return (val > array[index - 1]) ? (acc + 1) : acc;
    }
    case "part2": {
      if ([0, 1, array.length - 1].includes(index)) {
        return acc;
      }
      const previousSum = array[index - 2] + array[index - 1] + val;
      const currentSum = array[index - 1] + val + array[index + 1];
      // expected output from ../input.txt : 1311
      return (currentSum > previousSum) ? (acc + 1) : acc;
    }
    default:
      return acc;
  }
}, 0);

await Deno.stdout.write(new TextEncoder().encode(total.toFixed()));
