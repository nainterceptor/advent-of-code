import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

interface Point {
  x: number;
  y: number;
  sum: number;
}

interface PointTuple {
  from: Point;
  to: Point;
}

type Line = Point[];
type Matrix = Line[];

const data: PointTuple[] = stdin
  .split("\n")
  .filter((r) => r.length > 0)
  .map((r) => {
    const [from, to] = r.split("->").map((p) => {
      const [x, y] = p.split(",").map((u) => parseInt(u, 10));
      return { x, y } as Point;
    });
    return { from, to } as PointTuple;
  });

const matrixLength = data.reduce(
  (acc, val) =>
    Math.max(
      acc,
      ...[val.from, val.to].reduce(
        (iAcc, iVal) => [...iAcc, iVal.x, iVal.y],
        [] as number[],
      ),
    ),
  0,
) + 1;
const matrix: Matrix = Array.from({ length: matrixLength })
  .map((_, y) =>
    Array.from({ length: matrixLength }).map((
      _,
      x,
    ) => ({ x, y, sum: 0 } as Point))
  );

const [part] = Deno.args;

function forward({ from, to }: PointTuple) {
  const current = { ...from };
  const path: Point[] = [from];
  while (current.x !== to.x || current.y !== to.y) {
    if (current.x < to.x) {
      current.x++;
    } else if (current.x > to.x) {
      current.x--;
    }
    if (current.y < to.y) {
      current.y++;
    } else if (current.y > to.y) {
      current.y--;
    }
    path.push({ ...current });
  }

  return path;
}

function countIntersectionsOfMoreThan2Lines(matrix: Matrix) {
  return matrix
    .reduce((acc, line) => acc.concat(line), [])
    .reduce((acc, point) => acc + (point.sum >= 2 ? 1 : 0), 0);
}

function incrementMatrix(matrix: Matrix, tuples: PointTuple[]) {
  for (const pointTuple of tuples) {
    for (const point of forward(pointTuple)) {
      matrix[point.y][point.x].sum++;
    }
  }
}

switch (part) {
  case "part1": {
    incrementMatrix(
      matrix,
      data.filter(({ from, to }) => from.y === to.y || from.x === to.x),
    );
    // expected output from ../input.txt : 7438
    await Deno.stdout.write(
      new TextEncoder().encode(
        countIntersectionsOfMoreThan2Lines(matrix).toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    incrementMatrix(matrix, data);
    // expected output from ../input.txt : 21406
    await Deno.stdout.write(
      new TextEncoder().encode(
        countIntersectionsOfMoreThan2Lines(matrix).toFixed(),
      ),
    );
    break;
  }
  default:
}
