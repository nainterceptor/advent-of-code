use std::collections::HashSet;
use std::env;
use std::io::{self, BufRead};

type Line = Vec<u16>;

#[derive(Debug, Clone)]
struct Grid {
    size: u8,
    vertical: Vec<Line>,
    horizontal: Vec<Line>,
}

#[derive(Debug, Clone)]
struct Score {
    moves: u16,
    score: u32,
}

impl Grid {
    fn play(&self, draw: &Vec<u16>) -> Option<Score> {
        let mut drawn_numbers: HashSet<&u16> = HashSet::new();
        let all_lines: Vec<&Line> = self.vertical.iter().chain(self.horizontal.iter()).collect();
        for number in draw {
            drawn_numbers.insert(number);
            if (drawn_numbers.len() as u8) < self.size {
                continue;
            }
            if let Some(_first_intersected) = all_lines.iter().find(|l| {
                let line_hashset: HashSet<&u16> = l.iter().collect();
                drawn_numbers.intersection(&line_hashset).count() as u8 == self.size
            }) {
                let score = self.horizontal.iter().fold(0, |acc, cur| {
                    acc + cur.iter().fold(0, |local_sum, num| {
                        local_sum + (if drawn_numbers.contains(num) { 0 } else { *num })
                    })
                });
                return Some(Score {
                    moves: drawn_numbers.len() as u16,
                    score: score as u32 * *number as u32,
                });
            }
        }
        None
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();

    let draw: Vec<u16> = (&mut lines)
        .next()
        .unwrap()?
        .split(",")
        .filter_map(|s| s.parse().ok())
        .collect();

    let grids: Vec<Grid> = (&mut lines)
        .map(|l| l.unwrap())
        .fold(vec![], |mut acc, line| {
            if line.is_empty() || acc.is_empty() {
                acc.push(vec![]);
            } else {
                acc.last_mut().unwrap().push(line);
            }
            acc
        })
        .into_iter()
        .map(|g| {
            let size = (&g).len() as u8;
            let horizontal: Vec<Line> = (&g)
                .into_iter()
                .map(|r| r.split_whitespace().map(|u| u.parse().unwrap()).collect())
                .collect();
            let vertical: Vec<Line> = (0..size)
                .map(|index| horizontal.iter().map(|v| v[index as usize]).collect())
                .collect();
            Grid {
                size,
                horizontal,
                vertical,
            }
        })
        .collect();

    match env::args().nth(1).as_ref().map(|s| s.as_str()) {
        Some("part1") => {
            let Score { score, .. } = grids.iter().fold(
                Score {
                    score: 0,
                    moves: u16::MAX,
                },
                |acc, grid| {
                    let local_score = grid.play(&draw);
                    match local_score {
                        None => {
                            return acc;
                        }
                        Some(unwrapped_score) => {
                            if unwrapped_score.moves < acc.moves {
                                return unwrapped_score;
                            }
                            return acc;
                        }
                    }
                },
            );
            // expected output from ../input.txt : 65325
            print!("{}", score);
        }
        Some("part2") => {
            let Score { score, .. } = grids.iter().fold(
                Score {
                    score: 0,
                    moves: 0,
                },
                |acc, grid| {
                    let local_score = grid.play(&draw);
                    match local_score {
                        None => {
                            return acc;
                        }
                        Some(unwrapped_score) => {
                            if unwrapped_score.moves > acc.moves {
                                return unwrapped_score;
                            }
                            return acc;
                        }
                    }
                },
            );
            // expected output from ../input.txt : 4624
            print!("{}", score);
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
