import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

type Line = number[];
interface Grid {
  size: number;
  vertical: Line[];
  horizontal: Line[];
}

interface Score {
  moves: number;
  score: number;
}

const [inputDraw, ...inputGrids]: string[] = stdin
  .split("\n\n")
  .filter((r) => r.length > 0);

const draw = inputDraw.split(",").map((d) => parseInt(d, 10));

const grids: Grid[] = inputGrids
  .map((grid) => {
    const lines = grid
      .split("\n")
      .map((line) =>
        line.split(/\s+/).map((v) => parseInt(v.trim())).filter((v) =>
          !isNaN(v)
        ) as Line
      ) as Line[];
    return {
      size: lines.length,
      horizontal: lines,
      vertical: Array.from({ length: lines.length }).map((_, index) =>
        lines.map((l) => l[index])
      ) as Line[],
    } as Grid;
  });

const [part] = Deno.args;

function intersect(a: number[], b: number[]) {
  return a.filter(Set.prototype.has, new Set(b));
}

function play(grid: Grid, calledNumbers: number[]): Score | undefined {
  const drawnNumbers: number[] = [];
  const allLines = [...grid.vertical, ...grid.horizontal];
  for (const number of calledNumbers) {
    drawnNumbers.push(number);
    if (drawnNumbers.length < grid.size) {
      continue;
    }
    const intersected = allLines.find((l) =>
      intersect(l, drawnNumbers).length === grid.size
    );
    if (!intersected) {
      continue;
    }
    const score = grid.horizontal.reduce(
      (acc, curr) =>
        acc + curr.reduce((localSum, num) =>
          localSum + (drawnNumbers.includes(num)
            ? 0
            : num), 0),
      0,
    );
    return {
      moves: drawnNumbers.length,
      score: score * number,
    } as Score;
  }
}

switch (part) {
  case "part1": {
    const { score } = grids.reduce((acc: Score, grid) => {
      const localScore = play(grid, draw);
      if (!localScore) return acc;
      if (localScore.moves < acc.moves) {
        return localScore;
      }
      return acc;
    }, { moves: Infinity, score: 0 } as Score);
    // expected output from ../input.txt : 65325
    await Deno.stdout.write(
      new TextEncoder().encode(
        score.toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    const { score } = grids.reduce((acc: Score, grid) => {
      const localScore = play(grid, draw);
      if (!localScore) return acc;
      if (localScore.moves > acc.moves) {
        return localScore;
      }
      return acc;
    }, { moves: 0, score: 0 } as Score);
    // expected output from ../input.txt : 4624
    await Deno.stdout.write(
      new TextEncoder().encode(
        score.toFixed(),
      ),
    );
    break;
  }
  default:
}
