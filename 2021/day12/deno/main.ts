import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

type Vector = string[];

enum Kind {
  Large,
  Small,
}

interface Cave {
  kind: Kind;
  neighbors: Set<string>;
}

class Cave implements Cave {
  constructor(kind: Kind) {
    this.kind = kind;
    this.neighbors = new Set<string>();
  }
}

interface Graph {
  list: Map<string, Cave>;
}

class Graph implements Graph {
  constructor() {
    this.list = new Map<string, Cave>();
  }
  hasCave(name: string): boolean {
    return this.list.has(name);
  }
  addCave(name: string, kind: Kind) {
    this.list.set(name, new Cave(kind));
  }
  addCaveNeighbor(name: string, neighbor: string) {
    this.list.get(name)!.neighbors.add(neighbor);
    this.list.get(neighbor)!.neighbors.add(name);
  }

  static fromVectors(input: Vector[]): Graph {
    const graph = new Graph();
    for (const line of input) {
      for (const c of line) {
        if (!graph.hasCave(c)) {
          const firstLetter = c.charAt(0);
          const kind = firstLetter.toLowerCase() === firstLetter
            ? Kind.Small
            : Kind.Large;
          graph.addCave(c, kind);
        }
      }
      graph.addCaveNeighbor(line[0], line[1]);
    }
    return graph;
  }

  navigate(
    from: string,
    visited: string[],
    predicate: (_v: string[]) => boolean = () => true,
  ): number {
    if (from == "end") {
      return 1;
    }

    if (from == "start" && visited.length > 0) {
      return 0;
    }

    let node = this.list.get(from)!;
    // & (!part2 || is_small_cave_visited_twice(visited))
    if (
      Kind.Small == node.kind && visited.includes(from) && predicate(visited)
    ) {
      return 0;
    }
    visited.push(from);

    let result = [
      ...node
        .neighbors,
    ]
      .map((n: string) => this.navigate(n, visited, predicate))
      .reduce((acc: number, n: number) => n + acc, 0);

    visited.pop();
    return result;
  }
}

function isSmallCaveVisitedTwice(visited: string[]): boolean {
  const smallVisited = visited.filter((v) => {
    const firstChar = v.charAt(0);
    return firstChar.toLowerCase() === firstChar;
  });
  return smallVisited.length != [...new Set(smallVisited)].length;
}

const data: Vector[] = stdin
  .split("\n")
  .map((l) => l.split("-"));

const graph = Graph.fromVectors(data);

const [part] = Deno.args;
switch (part) {
  case "part1": {
    const visited: string[] = [];
    const result = graph.navigate("start", visited);
    // expected output from ../input.txt : 4011
    await Deno.stdout.write(
      new TextEncoder().encode(
        result.toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    const visited: string[] = [];
    const result = graph.navigate("start", visited, isSmallCaveVisitedTwice);
    // expected output from ../input.txt : 108035
    await Deno.stdout.write(
      new TextEncoder().encode(
        result.toFixed(),
      ),
    );
    break;
  }
  default:
}
