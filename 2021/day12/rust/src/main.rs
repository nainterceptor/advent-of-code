use std::collections::{HashMap, HashSet};
use std::env;
use std::io::{self, BufRead};

type Vector = [String; 2];

#[derive(Debug, PartialEq)]
enum Kind {
    Large,
    Small,
}

#[derive(Debug)]
struct Cave {
    kind: Kind,
    neighbors: HashSet<String>,
}

#[derive(Debug)]
struct Graph {
    list: HashMap<String, Cave>,
}

impl Cave {
    fn new(kind: Kind) -> Self {
        Self {
            kind,
            neighbors: HashSet::new(),
        }
    }
}

impl Graph {
    fn new() -> Self {
        Self {
            list: HashMap::new(),
        }
    }

    fn has_cave(&self, name: &String) -> bool {
        self.list.contains_key(name)
    }

    fn add_cave(&mut self, name: &String, kind: Kind) {
        self.list.insert(name.clone(), Cave::new(kind));
    }

    fn add_cave_neighbor(&mut self, name: &String, neighbor: &String) {
        self.list
            .get_mut(name)
            .unwrap()
            .neighbors
            .insert(neighbor.clone());
        self.list
            .get_mut(neighbor)
            .unwrap()
            .neighbors
            .insert(name.clone());
    }

    fn from_vectors(input: &Vec<Vector>) -> Self {
        let mut graph = Self::new();
        for line in input.iter() {
            for c in line.iter() {
                if !graph.has_cave(&c) {
                    let first_letter = c.chars().nth(0).unwrap();
                    graph.add_cave(
                        c,
                        if first_letter.is_lowercase() {
                            Kind::Small
                        } else {
                            Kind::Large
                        },
                    );
                }
            }
            graph.add_cave_neighbor(line.get(0).unwrap(), line.get(1).unwrap());
        }
        graph
    }

    fn navigate<F>(&self, from: &str, visited: &mut Vec<String>, predicate: &F) -> u32
    where
        F: Fn(&Vec<String>) -> bool,
    {
        if from == "end" {
            return 1;
        }

        if from == "start" && visited.len() > 0 {
            return 0;
        }

        let node: &Cave = &self.list[&from.to_string()];
        // & (!part2 || is_small_cave_visited_twice(visited))
        if Kind::Small == node.kind && visited.contains(&from.to_string()) && predicate(visited) {
            return 0;
        }
        visited.push(from.to_string());

        let result = node
            .neighbors
            .iter()
            .map(|n| self.navigate(n, visited, predicate))
            .sum();

        visited.pop();
        return result;
    }
}

fn is_small_cave_visited_twice(visited: &Vec<String>) -> bool {
    let visited_small: Vec<&String> = visited
        .iter()
        .filter(|v| v.chars().nth(0).unwrap().is_lowercase())
        .collect();
    let visited_small_hash: HashSet<&&String> = HashSet::from_iter(visited_small.iter().clone());

    visited_small.len() != visited_small_hash.len()
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let vectors: Vec<Vector> = io::stdin()
        .lock()
        .lines()
        .map(|l| l.unwrap())
        .map(|l| {
            l.split("-")
                .map(str::to_owned)
                .collect::<Vec<String>>()
                .try_into()
                .expect("slice with incorrect length")
        })
        .collect();

    let graph = Graph::from_vectors(&vectors);

    match env::args().nth(1).as_ref().map(|s| s.as_str()) {
        Some("part1") => {
            let mut visited = vec![];
            let result = graph.navigate("start", &mut visited, &|_| true);
            // expected output from ../input.txt : 4011
            print!("{}", result);
        }
        Some("part2") => {
            let mut visited = vec![];
            let result = graph.navigate("start", &mut visited, &is_small_cave_visited_twice);
            // expected output from ../input.txt : 108035
            print!("{}", result);
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
