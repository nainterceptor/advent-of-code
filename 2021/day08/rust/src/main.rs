use std::convert::TryInto;
use std::env;
use std::io::{self, BufRead};
use std::collections::HashSet;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input: Vec<[Vec<Vec<char>>; 2]> = io::stdin()
        .lock()
        .lines()
        .map(|l| l.unwrap())
        .map(|l| {
            l.splitn(2, "|")
                .map(|p| p.split_whitespace().map(|u| u.chars().collect()).collect())
                .collect::<Vec<Vec<Vec<char>>>>()
                .try_into()
                .unwrap()
        })
        .collect();

    match env::args().nth(1).as_ref().map(|s| s.as_str()) {
        Some("part1") => {
            let sum = input.into_iter().fold(0, |count, [_, output]|
                count + output.iter().filter(|r| [2, 3, 4, 7].contains(&r.len())).count()
            );
            // expected output from ../input.txt : 409
            print!("{}", sum);
        }
        Some("part2") => {
            let sum = input.into_iter().fold(0, |count, [input, output]| {
                let empty_vector: Vec<char> = vec![];
                let one: HashSet<&char> = HashSet::from_iter(input.iter().find(|i| i.len() == 2).unwrap_or(&empty_vector).iter());
                let four: HashSet<&char> = HashSet::from_iter(input.iter().find(|i| i.len() == 4).unwrap_or(&empty_vector).iter());
                count + output.iter().fold(0, |acc, out| {
                    let out_hash: HashSet<&char> = HashSet::from_iter(out.iter());
                    match out.len() {
                        2 => acc * 10 + 1,
                        3 => acc * 10 + 7,
                        4 => acc * 10 + 4,
                        5 => {
                            if out_hash.intersection(&one).count() == 2 {
                                acc * 10 + 3
                            } else if out_hash.intersection(&four).count() == 2 {
                                acc * 10 + 2
                            } else {
                                acc * 10 + 5
                            }
                        },
                        6 => {
                            if out_hash.intersection(&four).count() == 4 {
                                acc * 10 + 9
                            } else if out_hash.intersection(&one).count() == 2 {
                                acc * 10
                            } else {
                                acc * 10 + 6
                            }
                        },
                        7 => acc * 10 + 8,
                        _ => acc * 10,
                    }
                })
            }
            );
            // expected output from ../input.txt : 1024649
            print!("{}", sum);
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
