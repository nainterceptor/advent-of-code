import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

const data: number[] = stdin
  .split("\n")
  .filter((r) => r.trim().length > 0)
  .join()
  .split(",")
  .map((u) => parseInt(u, 10));

const max = Math.max(...data);
const min = Math.min(...data);

const [part] = Deno.args;

switch (part) {
  case "part1": {
    let cost = Infinity;
    for (let current = min; current <= max; current++) {
      const currentCost = data.reduce(
        (acc, row) => acc + Math.abs(row - current),
        0,
      );
      if (currentCost < cost) {
        cost = currentCost;
      }
    }
    // expected output from ../input.txt : 336120
    await Deno.stdout.write(
      new TextEncoder().encode(
        cost.toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    let cost = Infinity;
    for (let current = min; current <= max; current++) {
      const currentCost = data.reduce(
          (acc, row) => {
            let delta = Math.abs(row - current);
            for (let i = delta; i > 0; --i) {
              acc += i;
            }
            return acc;
          },
          0,
      );
      if (currentCost < cost) {
        cost = currentCost;
      }
    }
    // expected output from ../input.txt : 96864235
    await Deno.stdout.write(
      new TextEncoder().encode(
        cost.toFixed(),
      ),
    );
    break;
  }
  default:
}
