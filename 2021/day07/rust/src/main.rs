use std::env;
use std::io::{self, BufRead};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let data: Vec<u32> = io::stdin()
        .lock()
        .lines()
        .next()
        .unwrap()?
        .split(",")
        .filter_map(|s| s.parse().ok())
        .collect();

    let min = data.iter().min().unwrap();
    let max = data.iter().max().unwrap();

    match env::args().nth(1).as_ref().map(|s| s.as_str()) {
        Some("part1") => {
            let mut cost = u32::MAX;

            for current in *min..=*max {
                let current_cost = data.iter().fold(0, |acc, row| {
                    acc + (*row as isize - current as isize).abs() as u32
                });
                if current_cost < cost {
                    cost = current_cost;
                }
            }
            // expected output from ../input.txt : 336120
            print!("{}", cost);
        }
        Some("part2") => {
            let mut cost = u32::MAX;

            for current in *min..=*max {
                let current_cost = data.iter().fold(0, |acc, row| {
                    acc + (1..=(*row as isize - current as isize).abs() as u32)
                        .fold(0, |a, b| a + b)
                });
                if current_cost < cost {
                    cost = current_cost;
                }
            }
            // expected output from ../input.txt : 96864235
            print!("{}", cost);
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
