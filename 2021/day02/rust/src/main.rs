use std::env;
use std::io::{self, BufRead};
use std::str::FromStr;
use strum::EnumString;

#[derive(Debug, EnumString)]
enum Order {
    #[allow(non_camel_case_types)]
    forward,
    #[allow(non_camel_case_types)]
    up,
    #[allow(non_camel_case_types)]
    down,
}

struct Position {
    depth: u32,
    aim: u32,
    horizontal: u32,
}

type OrderTuple = (Order, u8);

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let input: Vec<OrderTuple> = io::stdin()
        .lock()
        .lines()
        .map(|l| l.unwrap())
        .map(|l| {
            let parts: Vec<&str> = l.split(" ").collect();
            (
                Order::from_str(parts[0]).unwrap(),
                parts[1].parse::<u8>().unwrap(),
            )
        })
        .collect();
    match part.as_ref() {
        "part1" => {
            let mut position = Position {
                depth: 0,
                aim: 0,
                horizontal: 0,
            };
            for (_, val) in input.iter().enumerate() {
                let (direction, steps) = val;
                match direction {
                    Order::down => position.depth += *steps as u32,
                    Order::up => position.depth -= *steps as u32,
                    Order::forward => position.horizontal += *steps as u32,
                };
            }
            let total = position.depth * position.horizontal;
            // expected output from ../input.txt : 2091984
            print!("{}", total);
        }
        "part2" => {
            let mut position = Position {
                depth: 0,
                aim: 0,
                horizontal: 0,
            };
            for (_, val) in input.iter().enumerate() {
                let (direction, steps) = val;
                match direction {
                    Order::down => position.aim += *steps as u32,
                    Order::up => position.aim -= *steps as u32,
                    Order::forward => {
                        position.depth += position.aim * *steps as u32;
                        position.horizontal += *steps as u32;
                    }
                };
            }
            let total = position.depth * position.horizontal;
            // expected output from ../input.txt : 2086261056
            print!("{}", total);
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
