import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

interface Order {
  forward: number;
  up: number;
  down: number;
}

interface Position {
  depth: number;
  aim: number;
  horizontal: number;
}

interface OrderTuple {
  direction: keyof Order;
  steps: number;
}

const data: OrderTuple[] = stdin
  .split("\n")
  .filter((r) => r.length > 0)
  .map((r) => {
    const [direction, steps] = r.split(" ");
    return { direction, steps: parseInt(steps, 10) } as OrderTuple;
  });

const [part] = Deno.args;

switch (part) {
  case "part1": {
    const { depth, horizontal } = data.reduce((acc, { direction, steps }) => {
      switch (direction) {
        case "down":
          return { ...acc, depth: acc.depth + steps };
        case "up":
          return { ...acc, depth: acc.depth - steps };
        case "forward":
          return { ...acc, horizontal: acc.horizontal + steps };
      }
    }, { depth: 0, aim: 0, horizontal: 0 } as Position);
    const total = depth * horizontal;
    // expected output from ../input.txt : 2091984
    await Deno.stdout.write(new TextEncoder().encode(total.toFixed()));
    break;
  }
  case "part2": {
    const { depth, horizontal } = data.reduce((acc, { direction, steps }) => {
      switch (direction) {
        case "down":
          return { ...acc, aim: acc.aim + steps };
        case "up":
          return { ...acc, aim: acc.aim - steps };
        case "forward":
          return {
            ...acc,
            depth: acc.depth + acc.aim * steps,
            horizontal: acc.horizontal + steps,
          };
      }
    }, { depth: 0, aim: 0, horizontal: 0 } as Position);
    const total = depth * horizontal;
    // expected output from ../input.txt : 2086261056
    await Deno.stdout.write(new TextEncoder().encode(total.toFixed()));
    break;
  }
  default:
}
