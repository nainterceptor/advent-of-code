use std::collections::HashMap;
use std::env;
use std::io::{self, BufRead};

type Polymer = String;
type PairCount = HashMap<(char, char), usize>;
type Rules = HashMap<(char, char), char>;

fn polymer_to_pair_count(polymer: &Polymer) -> PairCount {
    let polymer_chars: Vec<char> = polymer.chars().collect();
    polymer_chars
        .windows(2)
        .fold(PairCount::new(), |mut hashmap, pair| {
            *hashmap.entry((pair[0], pair[1])).or_default() += 1;
            hashmap
        })
}

fn bump_pairs(pairs: PairCount, rules: &Rules) -> PairCount {
    pairs
        .iter()
        .fold(PairCount::new(), |mut pairs, ((left, right), count)| {
            let inserted = *rules.get(&(*left, *right)).unwrap();
            *pairs.entry((*left, inserted)).or_default() += count;
            *pairs.entry((inserted, *right)).or_default() += count;
            pairs
        })
}

fn score_polymer(polymer: &Polymer, pairs: &PairCount) -> usize {
    let mut sum: HashMap<char, usize> = HashMap::new();

    for ((first, _), v) in pairs {
        *sum.entry(*first).or_insert(0) += v;
    }
    let last = polymer.chars().last().unwrap();
    *sum.entry(last).or_insert(0) += 1;

    let mut sum_vec: Vec<usize> = sum.into_values().collect();
    sum_vec.sort_unstable();
    sum_vec.last().unwrap() - sum_vec.first().unwrap()
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();

    let polymer: String = (&mut lines).nth(0).unwrap()?;

    let replacement_rules: Rules = (&mut lines)
        .skip(1)
        .map(|l| l.unwrap())
        .map(|r| {
            let (before, insert) = r.trim().split_once(" -> ").unwrap();

            (
                (
                    before.chars().nth(0).unwrap(),
                    before.chars().nth(1).unwrap(),
                ),
                insert.chars().nth(0).unwrap(),
            )
        })
        .collect();

    let pairs = polymer_to_pair_count(&polymer);

    match env::args().nth(1).as_ref().map(|s| s.as_str()) {
        Some("part1") => {
            let new_count = (0..10).fold(pairs, |p, _| bump_pairs(p, &replacement_rules));
            let score = score_polymer(&polymer, &new_count);
            // expected output from ../input.txt : 2321
            print!("{}", score);
        }
        Some("part2") => {
            let new_count = (0..40).fold(pairs, |p, _| bump_pairs(p, &replacement_rules));
            let score = score_polymer(&polymer, &new_count);
            // expected output from ../input.txt : 2399822193707
            print!("{}", score);
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
