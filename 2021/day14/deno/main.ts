import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

const [part] = Deno.args;

type Polymer = string;
type PairCount = Map<string, number>;
type Rules = Map<string, string>;

function slidingWindows(
  l: number,
  xs: string[],
  i = 0,
  out: string[] = [],
): string[] {
  return i > xs.length - l
    ? out
    : slidingWindows(l, xs, i + 1, [...out, xs.slice(i, i + l)] as string[]);
}

function polymerToPairCount(polymer: Polymer): PairCount {
  return slidingWindows(2, [...polymer]).reduce(
    (pairs: PairCount, [before, after]) => {
      const key = `${before}${after}`;
      pairs.set(key, (pairs.get(key) || 0) + 1);
      return pairs;
    },
    new Map() as PairCount,
  );
}

function bumpPairs(pairs: PairCount, rules: Rules): PairCount {
  return [...pairs.entries()].reduce(
    (newPairs: PairCount, [pair, count]: [string, number]) => {
      const [left, right] = [...pair];
      const inserted = rules.get(pair);
      newPairs.set(
        `${left}${inserted}`,
        (newPairs.get(`${left}${inserted}`) || 0) + count,
      );
      newPairs.set(
        `${inserted}${right}`,
        (newPairs.get(`${inserted}${right}`) || 0) + count,
      );
      return newPairs;
    },
    new Map() as PairCount,
  );
}

function scorePolymer(polymer: Polymer, pairs: PairCount): number {
  const sum = new Map();

  for (const [couple, count] of pairs.entries()) {
    const [first] = [...couple];
    sum.set(first, (sum.get(first) || 0) + count);
  }
  const last = polymer.charAt(polymer.length - 1);
  sum.set(last, (sum.get(last) || 0) + 1);

  const sumArray = [...sum.values()];
  sumArray.sort();
  return sumArray.pop() - sumArray.shift();
}

const [polymer, inputRules]: string[] = stdin
  .split("\n\n")
  .filter((r) => r.length > 0);

const rules: Rules = inputRules
  .split("\n")
  .reduce((acc, rule) => {
    const [before, insert] = rule.split(" -> ");
    acc.set(before, insert);
    return acc;
  }, new Map() as Rules);

const pairs = polymerToPairCount(polymer);

switch (part) {
  case "part1": {
    const newCount = Array.from({ length: 10 }).reduce(
      (p: PairCount, _) => bumpPairs(p, rules),
      pairs,
    );
    // expected output from ../input.txt : 2321
    await Deno.stdout.write(
      new TextEncoder().encode(
        scorePolymer(polymer, newCount).toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    const newCount = Array.from({ length: 40 }).reduce(
      (p: PairCount, _) => bumpPairs(p, rules),
      pairs,
    );
    // expected output from ../input.txt : 2399822193707
    await Deno.stdout.write(
      new TextEncoder().encode(
        scorePolymer(polymer, newCount).toFixed(),
      ),
    );
    break;
  }
  default:
}
