use std::env;
use std::io::{self, BufRead};

type ShortBinary = Vec<u8>;

fn sum_binaries(data: &Vec<ShortBinary>, input_length: &u8) -> Vec<u32> {
    let mut sum: Vec<u32> = vec![0; (*input_length).into()];
    for row in data {
        for index in 0..(*input_length).into() {
            sum[index] += row[index] as u32;
        }
    }
    sum
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let input: Vec<ShortBinary> = io::stdin()
        .lock()
        .lines()
        .map(|l| l.unwrap())
        .filter(|l| l.len() > 0)
        .map(|l| {
            let bits: Vec<&str> = l.split("").collect();
            bits.iter()
                .filter(|l| l.len() > 0)
                .filter_map(|b| b.parse::<u8>().ok())
                .collect()
        })
        .collect();

    let guess_length: u8 = (input[0].len()).try_into().unwrap();

    match part.as_ref() {
        "part1" => {
            let half_data_length: u32 = (input.len() / 2).try_into().unwrap();
            let binary_sum = sum_binaries(&input, &guess_length);
            let epsilon_rate = usize::from_str_radix(
                &binary_sum
                    .iter()
                    .map(|r| if r < &half_data_length { 1 } else { 0 })
                    .fold("".to_string(), |acc: String, bit: u8| {
                        acc + &bit.to_string()
                    }),
                2,
            )
            .unwrap();
            let gamma_rate = usize::from_str_radix(
                &binary_sum
                    .iter()
                    .map(|r| if r > &half_data_length { 1 } else { 0 })
                    .fold("".to_string(), |acc: String, bit: u8| {
                        acc + &bit.to_string()
                    }),
                2,
            )
            .unwrap();
            let total = epsilon_rate * gamma_rate;
            // expected output from ../input.txt : 3374136
            print!("{:?}", total);
        }
        "part2" => {
            let mut o2_rating = input.to_vec();
            let mut co2_rating = input.to_vec();
            for i in 0..guess_length {
                if o2_rating.len() > 1 {
                    let o2_index: u32 = sum_binaries(&o2_rating, &guess_length)[i as usize];
                    let half: f32 = o2_rating.len() as f32 / 2.0;
                    o2_rating = o2_rating
                        .to_vec()
                        .into_iter()
                        .filter(|r| {
                            r[i as usize]
                                == if (o2_index as f32) == half {
                                    1
                                } else if (o2_index as f32) > half {
                                    1
                                } else {
                                    0
                                }
                        })
                        .collect();
                }
            }
            for i in 0..guess_length {
                if co2_rating.len() > 1 {
                    let co2_index: u32 = sum_binaries(&co2_rating, &guess_length)[i as usize];
                    let half: f32 = co2_rating.len() as f32 / 2.0;
                    co2_rating = co2_rating
                        .to_vec()
                        .into_iter()
                        .filter(|r| {
                            r[i as usize]
                                == if (co2_index as f32) == half {
                                    0
                                } else if (co2_index as f32) < half {
                                    1
                                } else {
                                    0
                                }
                        })
                        .collect();
                }
            }

            let decimal_o2_rating = usize::from_str_radix(
                &o2_rating
                    .into_iter()
                    .flatten()
                    .fold("".to_string(), |acc: String, bit: u8| {
                        acc + &bit.to_string()
                    }),
                2,
            )
            .unwrap();

            let decimal_co2_rating = usize::from_str_radix(
                &co2_rating
                    .into_iter()
                    .flatten()
                    .fold("".to_string(), |acc: String, bit: u8| {
                        acc + &bit.to_string()
                    }),
                2,
            )
            .unwrap();
            // expected output from ../input.txt : 4432698
            let total = decimal_o2_rating * decimal_co2_rating;
            print!("{}", total);
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
