# Usage :

```bash
# part 1
deno run main.ts part1 < ../input.txt
# part 2
deno run main.ts part2 < ../input.txt
```

```bash
# part 1
cat ../input.txt | deno run main.ts part1
# part 2
cat ../input.txt | deno run main.ts part2
```
