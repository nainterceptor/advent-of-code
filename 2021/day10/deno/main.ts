import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

const [part] = Deno.args;

const PAIRS: Map<string, string> = new Map([
  ["(", ")"],
  ["[", "]"],
  ["{", "}"],
  ["<", ">"],
]);

const data: string[][] = stdin
  .split("\n")
  .map((l) => l.split("").filter((u) => u.length > 0));

switch (part) {
  case "part1": {
    const SCORES: Map<string, number> = new Map([
      [")", 3],
      ["]", 57],
      ["}", 1197],
      [">", 25137],
    ]);
    const score = data.reduce((s, line) => {
      const stack: string[] = [];
      for (const char of line) {
        if (PAIRS.has(char)) {
          stack.push(char);
        } else if (char !== PAIRS.get(stack.pop()!)) {
          return s + SCORES.get(char)!;
        }
      }
      return s;
    }, 0);
    // expected output from ../input.txt : 294195
    await Deno.stdout.write(
      new TextEncoder().encode(
        score.toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    const SCORES: Map<string, number> = new Map([
      [")", 1],
      ["]", 2],
      ["}", 3],
      [">", 4],
    ]);
    console.log(data.length);
    const scores = data.reduce((s: number[], line) => {
      const stack: string[] = [];
      breakableFor: {
        for (const char of line) {
          if (PAIRS.has(char)) {
            stack.push(char);
          } else if (char !== PAIRS.get(stack.pop()!)) {
            break breakableFor;
          }
        }
        return [
          ...s,
          stack.reduceRight(
            (score, char) => 5 * score + SCORES.get(PAIRS.get(char)!)!,
            0,
          ),
        ];
      }
      return s;
    }, []).sort((a: number, b: number) => a - b);
    console.log(scores.length);
    const score = scores[Math.floor(scores.length / 2)];
    // expected output from ../input.txt : 3490802734
    await Deno.stdout.write(
      new TextEncoder().encode(
        score.toFixed(),
      ),
    );
    break;
  }
  default:
}
