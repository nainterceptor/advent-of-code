use std::env;
use std::io::{self, BufRead};
use std::cmp::max;

type Coordinates = [usize; 2];
type Instruction = (char, usize);

struct Matrix (Vec<Vec<bool>>);

impl Matrix {
    fn from_dots(height: usize, width: usize, dots: Vec<Coordinates>) -> Self {
        let mut grid = Vec::with_capacity(height);

        for y in 0..height {
            grid.push(Vec::with_capacity(width));
            for _ in 0..width {
                grid[y].push(false);
            }
        }

        for [y,x] in dots {
            grid[y][x] = true;
        }

        Matrix (grid)
    }

    fn to_folded(self, (direction, location): Instruction) -> Self {
        let mut matrix = Matrix(self.0.into_iter().map(|u| u.to_vec()).collect());
        let grid = &mut matrix.0;
        match direction {
            'x' => {
                for x in 0.. location {
                    for y in 0.. grid.len() {
                        if let Some(_) = grid[y].get(2 * location - x) {
                            grid[y][x] = grid[y][x] || grid[y][2 * location - x];
                        }
                    }
                }
                grid.iter_mut().for_each(|line| { line.truncate(location) });
            },
            'y' => {
                for y in 0..location {
                    for x in 0..grid[0].len() {
                        if let Some(_) = grid.get(2 * location - y) {
                            grid[y][x] = grid[y][x] || grid[2 * location - y][x];
                        }
                    }
                }
                grid.truncate(location);
            },
            _ => panic!("Unauthorized direction")
        }
        matrix
    }

    fn count(&self) -> usize {
        return self.0.iter().map(|line| line.iter().map(|u| *u as usize).sum::<usize>()).sum::<usize>();

    }

    fn log(&self) {
        println!("{}", self.0.iter().map(|line| line.iter().map(|c| if *c { "#" } else {"."}).fold(String::new(), |a, b| a + b)).fold(String::new(), |a, b| a + "\n" + b.as_str()));
    }
}

const FOLD_ALONG: &str = "fold along";

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let (dots, instructions, height, width): (Vec<Coordinates>, Vec<Instruction>, usize, usize) = io::stdin()
        .lock()
        .lines()
        .map(|l| l.unwrap())
        .filter(|l| l.len() > 0)
        .fold((vec![], vec![], 0usize, 0usize), |(mut dots, mut instructions, mut height, mut width), input| {
            if input.starts_with(FOLD_ALONG) {
                let replaced_input = input.replace(FOLD_ALONG, "");
                let (direction, index) = replaced_input.split_once("=").unwrap();
                instructions.push((direction.trim().chars().nth(0).unwrap(), index.trim().parse().unwrap()));
            } else {
                let v: Vec<usize> = input.split(",").map(|l| l.trim().parse().unwrap()).collect();
                dots.push([v[1], v[0]]);
                height = max(height, v[1] + 1usize);
                width = max(width, v[0] + 1usize);
            }
            (dots, instructions, height, width)
        });
    let matrix = Matrix::from_dots(height, width, dots);

    match env::args().nth(1).as_ref().map(|s| s.as_str()) {
        Some("part1") => {
            let folded = matrix.to_folded(instructions[0]);
            // expected output from ../input.txt : 684
            print!("{}", folded.count());
        }
        Some("part2") => {
            let folded = instructions.iter().fold(matrix, |m, i| m.to_folded(*i));
            folded.log();
            // expected output from ../input.txt : JRZBLGKH
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
