use std::env;
use core::slice::Iter;
use std::collections::HashSet;
use std::collections::hash_set::{Intersection};
use std::collections::hash_map::{RandomState};
use std::io::{self, BufRead};

#[derive(Debug)]
struct Rucksack(HashSet<char>, HashSet<char>);
impl Rucksack {
    fn from_string(raw: &String) -> Rucksack {
        let half = raw.len() / 2;
        let (first, second) = raw.split_at(half);
        Rucksack(HashSet::from_iter(first.chars()), HashSet::from_iter(second.chars()))
    }
    fn get_intersection(&self) -> Intersection<'_, char, RandomState> {
        self.0.intersection(&self.1)
    }
    fn get_char_score(char: &char) -> u8 {
        let ascii = *char as u8;
        if ascii >= 97 && ascii <= 122 {
            ascii - 96 // Is lowercase
        } else if ascii >= 65 && ascii <= 90 {
            ascii - 64 + 26 // Is uppercase
        } else {
            0
        }
    }
    fn get_priorities(&self) -> u8 {
        self.get_intersection().fold(0, |acc, char| {
            Rucksack::get_char_score(char) + acc
        })
    }

    fn get_union(&self) -> HashSet<char> {
        self.0.union(&self.1).copied().collect()
    }

    fn chunk_intersection<'a>(rucksacks: Iter<'a, Rucksack>) -> HashSet<char> {
        rucksacks.map(|r| r.get_union()).reduce(|acc, r| acc.intersection(&r).copied().collect()).unwrap()
    }

    fn chunk_priorities(rucksacks: Iter<Rucksack>) -> u8 {
        Rucksack::chunk_intersection(rucksacks).iter().fold(0, |acc, char| {
            Rucksack::get_char_score(char) + acc
        })
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let list: Vec<Rucksack> = io::stdin()
        .lock()
        .lines()
        .filter_map(|e| e.ok())
        .map(|rucksack| {
            Rucksack::from_string(&rucksack)
        }).collect();
    // println!("{list:?}");

    match part.as_ref() {
        "part1" => {
            // expected output from ../input.txt : 8176
            let score = list.iter().fold(0u16, |acc, r| {
               acc + r.get_priorities() as u16
            });
            print!("{score}");
        }
        "part2" => {
            // expected output from ../input.txt : 2689
            let score = list.chunks(3).fold(0u16, |acc, r|
                acc + Rucksack::chunk_priorities(r.iter()) as u16
            );
            print!("{score}");
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
