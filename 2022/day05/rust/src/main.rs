use lazy_static::lazy_static;
use regex::Regex;
use std::env;
use std::io::{self, BufRead};

const NUMBER_OF_STACKS: u8 = 9;
const MIN_STACK_HEIGHT: u8 = 8;
lazy_static! {
    static ref INSTRUCTION_CAPTURE: Regex = Regex::new(r"\b(\d+)\b.*\b(\d)\b.*\b(\d)\b").unwrap();
}

type Stack = Vec<char>;
#[derive(Default, Debug)]
struct Supplies([Stack; NUMBER_OF_STACKS as usize]);
#[derive(Default, Debug)]
struct Instruction {
    quantity: u8,
    origin: u8,
    destination: u8,
}

impl Supplies {
    fn from_lines(mut lines: Vec<String>) -> Supplies {
        // Ignoring lower line, we will use NUMBER_OF_STACKS, else we can predict number of columns by measuring string, dividing by 3 and take upper unsigned
        lines.pop();
        Supplies(
            lines
                .into_iter()
                .rev()
                .fold(
                    vec![Vec::with_capacity(MIN_STACK_HEIGHT.into()); NUMBER_OF_STACKS.into()],
                    |mut acc: Vec<Vec<char>>, l| {
                        let chars = l.chars().collect::<Vec<char>>();
                        let splitted_list: Vec<Vec<&char>> = chars
                            .chunks(4)
                            .map(|chunk| {
                                chunk
                                    .iter()
                                    .filter(|l| !l.is_whitespace() && **l != '[' && **l != ']')
                                    .collect::<Vec<&char>>()
                            })
                            .collect();
                        let clean_list: Vec<Option<char>> = splitted_list
                            .into_iter()
                            .map(|chunk| match chunk.get(0) {
                                None => None,
                                Some(&&c) => Some(c),
                            })
                            .collect();
                        for (i, c) in clean_list.into_iter().enumerate() {
                            if let Some(char) = c {
                                acc[i].push(char);
                            }
                        }
                        acc
                    },
                )
                .try_into()
                .unwrap_or_else(|_| panic!("Expected a Vec of length {}", NUMBER_OF_STACKS)),
        )
    }
    fn execute_individually(&mut self, instruction: Instruction) -> &Self {
        let data = &mut self.0;
        let origin = &mut data[(instruction.origin - 1) as usize];
        let mut crates_to_move: Vec<char> = origin
            .drain((origin.len() - instruction.quantity as usize)..)
            .rev()
            .collect();
        let destination = &mut data[(instruction.destination - 1) as usize];
        destination.append(&mut crates_to_move);
        self
    }
    fn execute_many(&mut self, instruction: Instruction) -> &Self {
        let data = &mut self.0;
        let origin = &mut data[(instruction.origin - 1) as usize];
        let mut crates_to_move: Vec<char> = origin
            .drain((origin.len() - instruction.quantity as usize)..)
            .collect();
        let destination = &mut data[(instruction.destination - 1) as usize];
        destination.append(&mut crates_to_move);
        self
    }
    fn get_upper_line(&self) -> String {
        self.0.iter().fold(
            String::with_capacity(NUMBER_OF_STACKS.into()),
            |mut acc, stack| {
                let last = stack.last();
                if let Some(c) = last {
                    acc.push(*c);
                }
                acc
            },
        )
    }
}

impl Instruction {
    fn from_text(text: String) -> Instruction {
        let capture = INSTRUCTION_CAPTURE.captures(&text).unwrap();
        Instruction {
            quantity: capture[1].parse().unwrap(),
            origin: capture[2].parse().unwrap(),
            destination: capture[3].parse().unwrap(),
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let mut lines = io::stdin().lock().lines().filter_map(|e| e.ok());

    let mut supplies =
        Supplies::from_lines(lines.by_ref().take_while(|line| !line.is_empty()).collect());

    let instructions: Vec<Instruction> = lines
        .by_ref()
        .skip_while(|line| line.is_empty())
        .map(|l| Instruction::from_text(l))
        .collect();

    // println!("{supplies:?}{instructions:?}");

    match part.as_ref() {
        "part1" => {
            // expected output from ../input.txt : WSFTMRHPP
            for i in instructions.into_iter() {
                supplies.execute_individually(i);
            }
            print!("{}", supplies.get_upper_line());
        }
        "part2" => {
            // expected output from ../input.txt : GSLCMFBRP
            for i in instructions.into_iter() {
                supplies.execute_many(i);
            }
            print!("{}", supplies.get_upper_line());
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
