use std::env;
use std::io::{self, BufRead};

#[derive(Debug, Clone, Copy)]
struct Tree {
    size: u8,
    visible: bool,
    scenic_score: u32,
}
struct Map(Vec<Vec<Tree>>);

impl Tree {
    fn from_size(size: u8) -> Tree {
        Tree {
            size,
            visible: false,
            scenic_score: 1,
        }
    }
}

impl std::ops::BitXor for Tree {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self::Output {
        Tree {
            size: self.size ^ rhs.size,
            visible: self.visible ^ rhs.visible,
            scenic_score: self.scenic_score ^ rhs.scenic_score,
        }
    }
}

impl Map {
    fn from_lines(lines: Vec<String>) -> Map {
        Map(lines
            .into_iter()
            .map(|str| {
                str.chars()
                    .map(|c| Tree::from_size(c.to_digit(10).unwrap() as u8))
                    .collect()
            })
            .collect())
    }

    fn rotate_90(&mut self) {
        fn rotate<T: Copy + std::ops::BitXor + std::ops::BitXor<Output = T>>(
            matrix: &mut Vec<Vec<T>>,
        ) {
            for i in 0..matrix.len() {
                for j in i + 1..matrix[i].len() {
                    // Use XOR to swap the values of matrix[i][j] and matrix[j][i] without using a temporary variable.
                    matrix[i][j] = matrix[i][j] ^ matrix[j][i];
                    matrix[j][i] = matrix[i][j] ^ matrix[j][i];
                    matrix[i][j] = matrix[i][j] ^ matrix[j][i];
                }
            }

            for row in matrix.iter_mut() {
                row.reverse();
            }
        }
        rotate(&mut self.0);
    }

    fn calc_visibility_from_left(&mut self) {
        for line in self.0.iter_mut() {
            line[0].visible = true;
            let mut sizes = Vec::with_capacity(line.len());
            for val in line.iter_mut() {
                let max = sizes.iter().max().unwrap_or(&0);
                if *max < val.size {
                    val.visible = true;
                }
                sizes.push(val.size);
            }
        }
    }

    fn calc_scenic_from_left(&mut self) {
        for line in self.0.iter_mut() {
            let mut sizes = Vec::with_capacity(line.len());
            for val in line.iter_mut() {
                let mut number_visible = 0;
                for size in sizes.iter() {
                    number_visible += 1;
                    if *size >= val.size {
                        break;
                    }
                }
                val.scenic_score *= if number_visible > 0 {
                    number_visible
                } else {
                    1
                };
                sizes.insert(0, val.size);
            }
        }
    }

    fn count_visible(&self) -> u16 {
        self.0
            .iter()
            .map(|l| l.iter().map(|c| c.visible as u16).sum::<u16>())
            .sum()
    }

    fn max_scenic(&self) -> u32 {
        self.0
            .iter()
            .map(|l| l.iter().map(|c| c.scenic_score).max().unwrap())
            .max()
            .unwrap()
    }

    fn full_rotation(&mut self) {
        for _ in 0..4 {
            self.calc_visibility_from_left();
            self.calc_scenic_from_left();
            self.rotate_90();
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let mut map = Map::from_lines(io::stdin().lock().lines().filter_map(|e| e.ok()).collect());
    map.full_rotation();
    match part.as_ref() {
        "part1" => {
            // expected output from ../input.txt : 1560
            print!("{}", map.count_visible());
        }
        "part2" => {
            // expected output from ../input.txt : 252000
            print!("{}", map.max_scenic());
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
