use std::env;
use std::str::FromStr;
use std::io::{self, BufRead};
use std::vec::IntoIter;

#[derive(Debug)]
enum Instruction {
    ChangeDirectory(String),
    ListDirectory,
}

#[derive(Debug)]
enum Line {
    Instruction(Instruction),
    Directory(String),
    File(u32),
}

#[derive(Debug, Default)]
struct Directory {
    files_size: u64,
    sub: Vec<Directory>,
}

#[derive(Debug, Default)]
struct Computer {
    fs: Directory,
}


impl FromStr for Instruction {
    type Err = ();

    fn from_str(input: &str) -> Result<Instruction, Self::Err> {
        let mut parts = input.split_whitespace().skip(1);
        match parts.next() {
            Some("ls")  => Ok(Instruction::ListDirectory),
            Some("cd")  => Ok(Instruction::ChangeDirectory(parts.next().unwrap().into())),
            _      => Err(()),
        }
    }
}

impl Line {
    fn parse(input: String) -> Line {
        if input.starts_with("$") {
            Line::Instruction(Instruction::from_str(&input).unwrap())
        } else if input.starts_with("dir") {
            let dirname = input.split_whitespace().skip(1).next().unwrap();
            Line::Directory(dirname.into())
        } else {
            let size: u32 = input.split_whitespace().next().unwrap().parse().unwrap();
            Line::File(size)
        }
    }
}

impl Directory {
    fn build(mut self, lines: &mut IntoIter<Line>) -> Directory {
        while let Some(l) = lines.next() {
            match l {
                Line::Instruction(Instruction::ChangeDirectory(dir)) => {
                    if dir == "..".to_string() {
                        return self; // Recursivity break
                    } else {
                        self.sub.push(Directory::default().build(lines))
                    }
                },
                Line::Instruction(Instruction::ListDirectory) => continue, // We don't need to take action
                Line::File(size) => {
                    self.files_size += size as u64;
                },
                _ => continue,
            }
        }
        self
    }
    fn sum_size(&self) -> u64 {
        self.files_size + self.sub.iter().map(|d| d.sum_size()).sum::<u64>()
    }
    pub fn list_sizes<S>(&self, search: S) -> Vec<u64>
        where
            S: Fn(u64) -> bool + Copy,
    {
        let size = self.sum_size();
        let sizes: Vec<u64> = if search(size) { vec![size] } else { vec![] };
        self.sub.iter().fold(sizes, |mut acc, sub_dir| {
            acc.append(&mut sub_dir.list_sizes(search));
            acc
        })
    }
}

impl Computer {
    fn predict_filesystem(mut lines: Vec<Line>) -> Computer {
        let mut computer = Computer::default();
        computer.fs = computer.fs.build(&mut lines.into_iter());
        computer
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let computer = Computer::predict_filesystem(io::stdin()
        .lock()
        .lines()
        .skip(1)
        .filter_map(|e| e.ok()).map(|l| Line::parse(l)).collect());

    match part.as_ref() {
        "part1" => {
            // expected output from ../input.txt : 1428881
            print!("{}", computer.fs.list_sizes(|s| s <= 100_000).into_iter().sum::<u64>());
        }
        "part2" => {
            // expected output from ../input.txt : 10475598
            let total_size = computer.fs.sum_size();
            let to_free = total_size - (70_000_000 - 30_000_000);

            print!("{:?}", computer.fs.list_sizes(|s| s >= to_free).into_iter().min().unwrap());
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
