use std::env;
use std::io::{self, BufRead};
use std::str::FromStr;

#[derive(Debug)]
enum Instruction {
    Noop,
    Addx(i32),
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(input: &str) -> Result<Instruction, Self::Err> {
        let split = input.split_once(' ');
        match split {
            None => Ok(Instruction::Noop),
            Some(("addx", number)) => Ok(Instruction::Addx(number.parse().unwrap())),
            _ => Err(()),
        }
    }
}
#[derive(Debug)]
struct CPU(Vec<i32>);
impl CPU {
    fn with_capacity(size: usize) -> Self {
        let mut cycles = Vec::with_capacity(size);
        cycles.push(1);
        CPU(cycles)
    }

    fn execute(&mut self, instructions: &Vec<Instruction>) {
        let mut i = 0usize;
        for instruction in instructions {
            match instruction {
                Instruction::Noop => {
                    self.0.push(self.0[i]);
                    i += 1;
                }
                Instruction::Addx(x) => {
                    self.0.push(self.0[i]);
                    self.0.push(self.0[i + 1] + x);
                    i += 2;
                }
            }
        }
    }
    fn get_cycle_strength(&self, cycle_number: usize) -> i32 {
        cycle_number as i32 * self.0[cycle_number - 1]
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let mut instructions: Vec<Instruction> = io::stdin()
        .lock()
        .lines()
        .filter_map(|e| e.ok())
        .map(|e| Instruction::from_str(&e).unwrap())
        .collect();
    let mut cpu = CPU::with_capacity(240);
    cpu.execute(&instructions);
    match part.as_ref() {
        "part1" => {
            // expected output from ../input.txt : 14920
            print!(
                "{}",
                [20, 60, 100, 140, 180, 220]
                    .into_iter()
                    .map(|cycle| cpu.get_cycle_strength(cycle))
                    .sum::<i32>()
            );
        }
        "part2" => {
            // expected output from ../input.txt : BUCACBUZ
            let decoded_signal = cpu
                .0
                .iter()
                .enumerate()
                .map(|(i, value)| {
                    if (value - (i % 40) as i32).abs() < 2 {
                        '#'
                    } else {
                        '.'
                    }
                })
                .collect::<Vec<char>>()
                .chunks(40)
                .for_each(|pixels| {
                    println!("{}", pixels.iter().collect::<String>());
                });
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
