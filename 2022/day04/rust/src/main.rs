use std::env;
use std::io::{self, BufRead};
use std::ops::Range;

#[derive(Debug)]
struct AssignmentPair(Range<u8>, Range<u8>);

impl AssignmentPair {
    fn from_string(str: String) -> AssignmentPair {
        let mut pair: Vec<Range<u8>> = str
            .split(",")
            .take(2)
            .map(|a| {
                let mut assignment: Vec<u8> =
                    a.split("-").take(2).map(|b| b.parse().unwrap()).collect();
                let start = assignment.remove(0);
                let end = assignment.remove(0);
                start..end
            })
            .collect();
        AssignmentPair(pair.remove(0), pair.remove(0))
    }
    fn has_full_overlap(&self) -> bool {
        self.0.start <= self.1.start && self.0.end >= self.1.end
            || self.1.start <= self.0.start && self.1.end >= self.0.end
    }
    fn has_partial_overlap(&self) -> bool {
        (self.0.start <= self.1.end && self.0.end >= self.1.start)
            || (self.1.start <= self.0.end && self.1.end >= self.0.start)
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let list: Vec<AssignmentPair> = io::stdin()
        .lock()
        .lines()
        .filter_map(|e| e.ok())
        .map(|ap| AssignmentPair::from_string(ap))
        .collect();

    match part.as_ref() {
        "part1" => {
            // expected output from ../input.txt : 450
            let score = list.into_iter().filter(|ap| ap.has_full_overlap()).count();
            print!("{score}");
        }
        "part2" => {
            // expected output from ../input.txt : 837
            let score = list
                .into_iter()
                .filter(|ap| ap.has_partial_overlap())
                .count();
            print!("{score}");
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
