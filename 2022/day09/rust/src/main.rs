use std::env;
use std::io::{self, BufRead};
use std::str::FromStr;

#[derive(Debug)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(input: &str) -> Result<Direction, Self::Err> {
        match input {
            "L" => Ok(Direction::Left),
            "R" => Ok(Direction::Right),
            "U" => Ok(Direction::Up),
            "D" => Ok(Direction::Down),
            _ => Err(()),
        }
    }
}

#[derive(Debug)]
struct Instruction(Direction, u8);
impl FromStr for Instruction {
    type Err = ();

    fn from_str(input: &str) -> Result<Instruction, Self::Err> {
        let (dir, number) = input.split_once(' ').unwrap();
        Ok(Instruction(
            Direction::from_str(dir)?,
            number.parse().ok().expect("Simple parse"),
        ))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Point(i16, i16);

impl std::ops::Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self(self.0 - other.0, self.1 - other.1)
    }
}

impl std::ops::Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0, self.1 + other.1)
    }
}

impl std::ops::AddAssign for Point {
    fn add_assign(&mut self, other: Self) {
        *self = Self(self.0 + other.0, self.1 + other.1);
    }
}

impl Point {
    fn new(x: i16, y: i16) -> Self {
        Self(x, y)
    }
    fn signum(&self) -> Self {
        Self(self.0.signum(), self.1.signum())
    }

    fn abs(&self) -> Self {
        Self(self.0.abs(), self.1.abs())
    }

    fn max(&self) -> i16 {
        self.0.max(self.1)
    }
    fn from_direction(direction: &Direction) -> Self {
        match direction {
            Direction::Left => Point::new(-1, 0),
            Direction::Right => Point::new(1, 0),
            Direction::Up => Point::new(0, -1),
            Direction::Down => Point::new(0, 1),
        }
    }
}

struct Simulation {}

impl Simulation {
    fn process(instructions: Vec<Instruction>, size: usize) -> u16 {
        let mut tail = std::collections::HashSet::new();
        let mut knots = vec![Point::new(0, 0); size + 1];

        tail.insert(*knots.last().unwrap());
        for instruction in instructions {
            let dir = Point::from_direction(&instruction.0);

            for _ in 0..instruction.1 {
                knots[0] += dir;

                for i in 1..knots.len() {
                    let diff = knots[i - 1] - knots[i];
                    if diff.abs().max() <= 1 {
                        continue;
                    }

                    knots[i] += diff.signum();
                }
                tail.insert(*knots.last().unwrap());
            }
        }

        tail.len() as u16
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let mut instructions: Vec<Instruction> = io::stdin()
        .lock()
        .lines()
        .filter_map(|e| e.ok())
        .map(|e| Instruction::from_str(&e).unwrap())
        .collect();

    match part.as_ref() {
        "part1" => {
            // expected output from ../input.txt : 6212
            print!("{}", Simulation::process(instructions, 1));
        }
        "part2" => {
            // expected output from ../input.txt : 2522
            print!("{}", Simulation::process(instructions, 9));
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
