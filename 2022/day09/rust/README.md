# Usage :

```bash
# part 1
cargo run --release part1 < ../input.txt
# part 2
cargo run --release part2 < ../input.txt
```

```bash
# part 1
cat ../input.txt | cargo run --release part1
# part 2
cat ../input.txt | cargo run --release part2
```
